<?php

namespace FpDbTest;

use Exception;
use mysqli;

/**
 *
 */
class Database implements DatabaseInterface
{
    /**
     * @var mysqli
     */
    private mysqli $mysqli;

    /**
     * @param mysqli $mysqli
     */
    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    /**
     * @param string $query -  MySQL query
     * @param array $args parameters
     * @return string formatted  query
     */
    public function buildQuery(string $query, array $args = []): string
    {
        try {
            $parametred_query = preg_replace_callback('/\?([dfa#]*)/', function ($matches) use ($query, $args) {
                static $position = 0;

                $modifier = $matches[1];
                if ($modifier == '')
                    $value = $this->quote($args[$position]);
                if ($modifier == 'd')
                    $value = (int)$args[$position];
                if ($modifier == 'f')
                    $value = (float)$args[$position];
                if ($modifier == 'a')
                    $value = $this->join($args[$position], '\'');
                if ($modifier == '#')
                    $value = $this->join($args[$position], '`');
                $position++;
                return $value;
            }, $query);
            return preg_replace_callback('/\{(.*?)\}/', function ($matches) use ($query, $args) {
                return in_array('[#s#k#i#p#' . static::getSkipper() . ']', $args, true) ? '' : $matches[1];
            }, $parametred_query);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param $array - string|array array  names or simple string
     * @param $quote -  string quote to be escaped. in MySQL, it is typewritten backtick, and for strings it is simple single quotes
     * @return string
     */
    public function join($array, $quote = '\'')
    {
        if (!is_array($array))
            $array = [$array];
        $newvalues = [];
        foreach ($array as $index => $item) {
            if (is_integer($index))
                $newvalues[$index] = $this->quote($item, $quote);
            else
                $newvalues[$index] = $this->quote($index, '`') . ' = ' . $this->quote($item);
        }
        return implode(', ', $newvalues);
    }

    /**
     * @return false|string returns a special value similar to unique_id
     */
    public static function getSkipper()
    {
        return md5_file(__FILE__);/* the probability of encountering your hash in the request text is negligible compared to the significance of this event */
    }

    /**
     * @return string allows you to skip a conditional block
     */
    public function skip()
    {
        return '[#s#k#i#p#' . static::getSkipper() . ']';
        //    throw new Exception();
    }

    /**
     * @param $item
     * @param $quote
     * @return int|string|void converts PHP type to MySQL
     */
    protected function quote($item, $quote = '\'')
    {
        $type = mb_strtolower(gettype($item));

        if ($type == 'null')
            return 'NULL';
        if ($type == 'string')
            //   return $quote . str_replace("\'", "\\\\'", $item) . $quote;
            return $quote . $this->mysqli->real_escape_string($item) . $quote;//PDO::quote( $item, $type = PDO::PARAM_STR);
        if ($type == 'integer')
            return (int)$item;
        if ($type == 'boolean')
            return (int)$item;
        throw new Exception('invalid argument type');
    }
}
