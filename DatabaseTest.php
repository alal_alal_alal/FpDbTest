<?php

namespace FpDbTest;

use Exception;

class DatabaseTest
{
    private DatabaseInterface $db;
    public $verbose;
    public $extension = ['mysqli', 'mbstring'];

    public function __construct(DatabaseInterface $db)
    {
        $this->db = $db;
        $this->checkExtensions();
    }

    public function trimSpace($str){
        return  preg_replace('/\s+/', ' ', $str );
    }
    public function testBuildQuery(): void
    {
        $results = [];

        $results[] = $this->db->buildQuery('SELECT name FROM users WHERE user_id = 1');

        $results[] = $this->db->buildQuery(
            'SELECT * FROM users WHERE name = ? AND block = 0',
            ['Jack']
        );

        $results[] = $this->db->buildQuery(
            'SELECT ?# FROM users WHERE user_id = ?d AND block = ?d',
            [['name', 'email'], 2, true]
        );

        $results[] = $this->db->buildQuery(
            'UPDATE users SET ?a WHERE user_id = -1',
            [['name' => 'Jack', 'email' => null]]
        );

        foreach ([null, true] as $block) {
            $results[] = $this->db->buildQuery(
                'SELECT name FROM users WHERE ?# IN (?a){ AND block = ?d}',
                ['user_id', [1, 2, 3], $block ?? $this->db->skip()]
            );
        }

        $correct = [
            'SELECT name FROM users WHERE user_id = 1',
            'SELECT * FROM users WHERE name = \'Jack\' AND block = 0',
            'SELECT `name`, `email` FROM users WHERE user_id = 2 AND block = 1',
            'UPDATE users SET `name` = \'Jack\', `email` = NULL WHERE user_id = -1',
            'SELECT name FROM users WHERE `user_id` IN (1, 2, 3)',
            'SELECT name FROM users WHERE `user_id` IN (1, 2, 3) AND block = 1',
        ];
        $space = str_repeat(' ', 8);
        if ($this->verbose)
        foreach ($correct as $index => $item) {
            $status = ($results[$index] == $item) ;
           // $status = ($this->trimSpace($results[$index]) == $this->trimSpace($item)) ;
            echo "[$index] " . ($status ? $this->HiglightConsoleText('OK', 's') : $this->HiglightConsoleText('ERR', 'e')) . ' ' . $results[$index] . ($status ? '' : '  а должно быть' . PHP_EOL . $space . $item) . PHP_EOL;
        }
        /*     $results[] = $this->db->buildQuery(
                 'SELECT ?# FROM users WHERE user_id = ?d AND block = ?d',
                 [['name', 'email', function () {
             // this argument type should throw an exception
         }], 2]
             );*/
        if ($results !== $correct) {
            throw new Exception('Failure.');
        }
    }

    public function HiglightConsoleText($str, $type = 'i')
    {
        switch ($type) {
            case 'e': //error
                echo "\033[31m$str \033[0m";
                break;
            case 's': //success
                echo "\033[32m$str \033[0m";
                break;
            case 'w': //warning
                echo "\033[33m$str \033[0m";
                break;
            case 'i': //info
                echo "\033[36m$str \033[0m";
                break;
            default:
                # code...
                break;
        }
    }

    private function checkExtensions()
    {
        foreach ($this->extension as $index => $item) {
            if (!extension_loaded($item))
                throw new Exception('"' . $item . '" is not loaded! Please add it into <php.ini>');
        }
    }
}

